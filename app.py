
from flask import Flask, request, abort
#天氣必要套件
import requests
import json

#line必要套件
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,ImageMessage,ImageSendMessage
)

#======這裡是呼叫的檔案內容=====
# from message import *
# from new import *
# from Function import *
#======這裡是呼叫的檔案內容=====

#======python的函數庫==========
import tempfile, os
import datetime
import time
#匯入定時module
#from apscheduler.schedulers.blocking import BlockingScheduler

#======python的函數庫==========

app = Flask(__name__)
static_tmp_path = os.path.join(os.path.dirname(__file__), 'static', 'tmp')
# Channel Access Token
line_bot_api = LineBotApi('tXcCnRRk9DoPQsysmycFgCEyk7tEZsBGWOFa2XnF30f7TiXza+JPMV7jWWSAfmOp3cWD90bule6eKHeUjjqGZbJQVtRAi6NMfT6e7Sf0BtiK34vkPx7j6RHelmgAXzecHgEiHMhjqiBPQXZ+sA6qRgdB04t89/1O/w1cDnyilFU=')
# Channel Secret
handler = WebhookHandler('3b495d0eb4ac6e65026ca02f25ec3c28')

# 監聽所有來自 /callback 的 Post Request
@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']
    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)
    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'

# 處理訊息
@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    msg = event.message.text
    today = str(datetime.date.today().strftime('%A'))


    if '今天踢嗎' in msg:
        today = datetime.date.today().strftime('%Y-%m-%d %A')
        message = TextSendMessage(text = str(today)+'\n20 : 20 ~ 22 : 20\n➡️ (!)統計至晚間八點，滿10人開團(!)\n➡️ 上場踢球前請勿佩戴任何禁止在足球比賽時出現的飾品以保障場上球員的安全。\n☆ 為避免受傷請提早到場熱身\n★ 已經報名但無法到場請提早通知。\n1 .\n2 .\n3 .\n4 .\n5 .\n6 .\n7 .\n8 .\n9 .\n10 .\n11 .\n12 .\n13 .\n14 .\n15 .\n16 . ') 
        line_bot_api.reply_message(event.reply_token, message)

    elif '明天踢嗎' in msg:
        tomorrow = (datetime.date.today() + datetime.timedelta(days = 1)).strftime('%Y-%m-%d %A')
        message = TextSendMessage(text = str(tomorrow)+'\n20 : 20 ~ 22 : 20\n➡️ (!)統計至晚間八點，滿10人開團(!)\n➡️ 上場踢球前請勿佩戴任何禁止在足球比賽時出現的飾品以保障場上球員的安全。\n☆ 為避免受傷請提早到場熱身\n★ 已經報名但無法到場請提早通知。\n1 .\n2 .\n3 .\n4 .\n5 .\n6 .\n7 .\n8 .\n9 .\n10 .\n11 .\n12 .\n13 .\n14 .\n15 .\n16 . ') 
        line_bot_api.reply_message(event.reply_token, message)

    elif '在哪' in msg: 
        profile = event.source.user_id
        line_bot_api.push_message(profile, TextSendMessage(text='比賽場地在 大佳河濱公園 https://goo.gl/maps/52J4uDGRsLoSBVBd8'))

    # 主動post 功能未完成
    # 比對今天是星期幾主動揪團
    # today = str(datetime.date.today().strftime('%A'))
    # if today == 'Monday'
    # profile = event.source.user_id
    # # line_bot_api.push_message(profile, TextSendMessage(text=""))
    # else:

    # 輸出時間進行自動推播
    # def job():
    #     tomorrow = (datetime.date.today() + datetime.timedelta(days = 1)).strftime('%Y-%m-%d %A')
    #     message = TextSendMessage(text = str(tomorrow)+'\n20 : 20 ~ 22 : 20\n➡️ (!)統計至晚間八點，滿10人開團(!)\n➡️ 上場踢球前請勿佩戴任何禁止在足球比賽時出現的飾品以保障場上球員的安全。\n☆ 為避免受傷請提早到場熱身\n★ 已經報名但無法到場請提早通知。\n1 .\n2 .\n3 .\n4 .\n5 .\n6 .\n7 .\n8 .\n9 .\n10 .\n11 .\n12 .\n13 .\n14 .\n15 .\n16 . ') 
    #     line_bot_api.reply_message(event.reply_token, message)
    # # BlockingScheduler
    # scheduler = BlockingScheduler()
    # scheduler.add_job(job, 'cron', day_of_week='0-6', hour=14, minute=40)
    # scheduler.start()

    # 得到userID
    #     profile = event.source.user_id
    #     message = TextSendMessage(text = str(profile))
    #     line_bot_api.reply_message(event.reply_token, message)

    # # 參考下列網站使用try
    # # https://xiaosean.github.io/chatbot/2018-04-19-LineChatbot_usage/
    # try:
    #     line_bot_api.push_message(U8ed7a1925757ffbbcf949cf7531e503c, TextSendMessage(text='成功了'))
    # except LineBotApiError as e:
    #     # error handle
    #     raise e




import os
if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
